# coding=utf8
import os
import json
import requests


def load_points(file_path):
    points = []
    with open(file_path) as file:
        for line in file.readlines()[1:]:
            words = line.strip().split(",")
            points.append({"x1": float(words[0]), "y1": float(words[1]), "x2": float(words[2]), "y2": float(words[3])})
    return points


def create_request(points, count, route_mode, traffic_mode, transport, alternative, locale, output):
    requests_list = []
    for i in range(0, count):
        routing_data = {
              "points": [
                {
                  "lon": points[i]['x1'],
                  "lat": points[i]['y1'],
                  "type": "walking",
                  "idx": 0
                },
                {
                  "lon": points[i]['x2'],
                  "lat": points[i]['y2'],
                  "type": "walking",
                  "idx": 1
                }
              ],
              "route_mode": route_mode,
              "traffic_mode": traffic_mode,
              "transport": transport,
              "output": output,
              "alternative": alternative,
              "locale": locale

            }
        requests_list.append(routing_data)
    return requests_list


def run_post(url, requests_list):
    routes_count = len(requests_list)
    distance_list = []
    duration_list = []
    for i, request in enumerate(requests_list):
        request_body = json.dumps(request)
        resp = requests.post(url, data=request_body)
        resp.raise_for_status()

        json_resp = resp.json()
        distance = json_resp["routes"][0]["distance"]
        duration = json_resp["routes"][0]["duration"]
        distance_list.append(distance)
        duration_list.append(duration)


points = load_points('nsk.csv')
requests_list = create_request(points, 100, 'fastest', 'jam', 'car', 1, 'ru', 'summary')
url = 'server_name//routing/7.0.0'
run_post(url, requests_list)
