## Скрипт для проверки новых типов запросов для Routing api

1. какие параметры будут меняться в запросе (подавать методу)
 - route_mode (shortest /  fastest)
 - traffic_mode (jam / statistics)
 - transport (car / pedestrian / taxi / bicycle / truck)
 - output (summary / geometry / detailed)
 - в params для пешиков true/false для use_indoor, use_instruction, для truck свои параметры. Проверить, какие есть еще параметры.

2. нужно будет проверять ответ: distance, duration, route_id и еще какие-нибудь значимые параметры.
